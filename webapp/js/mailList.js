//  第三个参数改成回调，把接收到的值回调一手
//  之前是ajax包了一个promise在里面，然后ajax return 一个 promise对象回去
// 后来发现不对，ajax是异步的啊！返回值固然是不好的！
// 那我这种异步里包同步的操作就不好
// 但是我反过来，promise包ajax，这样不就是同步包异步了吗，我都觉得自己有点机智
const base = 'http://localhost:5678/'
function AJAX_callback(method = 'POST', url = base, ID, tar) {
        let xhr = new XMLHttpRequest()
        let resData = ''
        url = base + url
        if(!xhr) {
            console.warn('xhr创建失败')
            reject(-1)
        }
        if(method == 'GET') {
            xhr.open('GET', url)
        } else {
            xhr.open('POST', url)
        }
    
        xhr.send(JSON.stringify({'id':ID}))
        
        xhr.onreadystatechange = () => {
            if(xhr.readyState == 4 && xhr.status >= 200 && xhr.status < 400) {
                res = JSON.parse(xhr.responseText)
                console.log('请求-响应完毕，响应数据', res)
                for(let i = 1; i < 10; i ++) {
                    // console.log(IDs[i])
                    if(i > res.length)
                    tar.IDs[i].innerHTML = tar.usernames[i].innerHTML = tar.phonenums[i].innerHTML = `<div style='color:#222'>(待添加)</div>`
                    else {
                        tar.IDs[i].innerHTML = res[i-1].user_id
                        tar.usernames[i].innerHTML = res[i-1].username
                        tar.phonenums[i].innerHTML = res[i-1].phonenum
                    }
                }   
            } else {
                console.log('失败')
            }
        }
        
    }

 


// function AJAX_promise(method = 'POST', url = base) {
//     const promise = new Promise((resolve, reject) => {
//         let xhr = new XMLHttpRequest()
//         url = base + url
//         if(!xhr) {
//             console.warn('xhr创建失败')
//             reject()
//         }
//         if(method == 'GET') {
//             xhr.open('GET', url)
//         } else {
//             xhr.open('POST', url)
//         }
//         xhr.send()
        
//         xhr.onreadystatechange = () => {
//             if(xhr.readyState == 4 && xhr.status == 200) {
//                 let resData = JSON.parse(xhr.responseText)
//                 console.log('请求-响应完毕，响应数据', resData)
//                 resolve(resData)                
//             } else {
//                 reject(xhr.status)
//             }
//         }
//         return promise
//     })
// }