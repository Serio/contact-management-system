const baseURL = 'http://localhost:5678/'
function AJAX(method, URL = baseURL, data) {
    let xhr = new XMLHttpRequest()
    URL = baseURL + URL
    if(!xhr) {
       console.error('请求出错')
       return null
    }
    if(method === 'POST') {
        xhr.open('POST', URL)
        console.log(data)
        xhr.send(JSON.stringify(data))
    } else if(method === 'GET') {
        xhr.open('GET', URL)
        xhr.send()
    }

    xhr.onreadystatechange = () => {
        if(xhr.readyState == 4 && xhr.status == 200){
            let resData = JSON.parse(xhr.responseText)
            console.log('请求-响应完毕', resData)
            if(resData != -1) {
                sessionStorage.setItem('id',resData)
                sessionStorage.setItem('username', data.username)
                sessionStorage.setItem('password', data.password)
                if(data.phonenum)
                    sessionStorage.setItem('phonenum', data.phonenum)
                window.location.href = "./mailList.html"    
            } else {
                alert('账号密码错误，请重新输入')
            }
            // return resData
        } else {
            console.warn('没有正确地响应')
            // return null
        }
    }
}
