//  基类
class BaseModel {
    constructor(data, msg) {
        if(data) {
            this.data = data
        }
        if(msg) {
            this.msg = msg
        }
    }
}
// 成功模型
class SuccessModel extends BaseModel {
    constructor(data, msg) {
        super(data, msg)
        this.err = 0
    }
}

// 失败模型
class ErrorModel extends BaseModel {
    constructor(data, msg) {
        super(data, msg)
        this.err = -1
    }
}

module.exports = {
    SuccessModel,
    ErrorModel
}
