const mysql = require('mysql')
// 连接
const connection = mysql.createConnection({
    host: 'localhost',
    user: 'root',
    password: 'root',
    port: 3306,
    database: 'syymaillist'
})

connection.connect()

// 执行sql
// const sql = `select * from users`
// connection.query(sql, (err, result) => {
//     if(err) {
//         console.log(err)
//         return
//     }
//     console.log('result', result)
// })

function executeSQL(sql) {
    const promise = new Promise((resolve, reject) => {
        connection.query(sql, (err, res) => {
            if(err) {
                reject(err)
                return
            }
            resolve(res)
        })
    })
    return promise
}



module.exports = {
    executeSQL
}